package exercise;

import java.util.Map.Entry;
import java.util.Set;

// BEGIN
public class App {
    public static void swapKeyValue(KeyValueStorage storage) {
        for (Entry<String, String> kv: storage.toMap().entrySet()) {
            storage.unset(kv.getKey());
            storage.set(kv.getValue(), kv.getKey());
        }
    }
}
// END
