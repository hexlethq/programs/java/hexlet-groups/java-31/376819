package exercise;

import java.util.Map;

// BEGIN
public class FileKV implements KeyValueStorage {
    String path;
    Map<String, String> kv;

    public FileKV() {

    }

    public FileKV(String path, Map<String, String> kv) {
        this.path = path;
        this.kv = kv;
        writeFile(this.kv);
    }

    @Override
    public void set(String key, String value) {
        Map<String, String> map = readFile();
        map.put(key, value);
        writeFile(map);
    }

    @Override
    public void unset(String key) {
        Map<String, String> map = readFile();
        map.remove(key);
        writeFile(map);
    }

    @Override
    public String get(String key, String defaultValue) {
        Map<String, String> map = readFile();
        return map.getOrDefault(key,defaultValue);
    }

    @Override
    public Map<String, String> toMap() {
        return readFile();
    }

    private Map<String, String> readFile() {
        String JsonData = Utils.readFile(path);
        return Utils.unserialize(JsonData);
    }

    private void writeFile(Map<String, String> map) {
        String JsonData = Utils.serialize(map);
        Utils.writeFile(path, JsonData);
    }
}
// END
