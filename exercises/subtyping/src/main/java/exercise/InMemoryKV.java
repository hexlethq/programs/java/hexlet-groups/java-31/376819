package exercise;

import java.util.Map;
import java.util.HashMap;

// BEGIN
public class InMemoryKV implements KeyValueStorage {
    public Map<String, String> kv = new HashMap<>();

    public InMemoryKV() {}

    public InMemoryKV(Map<String, String> kv) {
        this.kv.putAll(kv);
    }

    @Override
    public void set(String key, String value) {
        kv.put(key, value);
    }

    @Override
    public void unset(String key) {
        this.kv.remove(key);
    }

    @Override
    public String get(String key, String defaultValue) {
        return kv.getOrDefault(key, defaultValue);
    }

    @Override
    public Map<String, String> toMap() {
        Map<String, String> map = new HashMap<>();
        for (String key: kv.keySet()) {
            map.put(key, kv.get(key));
        }
        return map;
    }
}
// END
