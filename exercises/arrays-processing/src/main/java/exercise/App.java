package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int getIndexOfMaxNegative(int[] arr) {
        int maxNegative = Integer.MIN_VALUE;
        int index = -1;
        for (int i = 0; i < arr.length; i++){
            if (arr[i] > maxNegative && arr[i] < 0) {
                maxNegative = arr[i];
                index = i;
            }
        }
        return index;
    }

    public static int[] getElementsLessAverage(int[] numbers) {
        if (numbers.length !=0) {
            double sum = 0.0;
            int i = 0;
            for (int a : numbers) {
                sum += a;
            }
            double avgSum = sum / numbers.length;
            for (int a : numbers) {
                i = a <= avgSum ? i + 1 : i;
            }
            int[] arr = new int[i];
            i = 0;
            for (int a : numbers)  {
                //arr[i] = a < avgSum ? a : i;
                if (a <= avgSum) {
                    arr[i] = a;
                    i++;
                }
            }
            return arr;
        } else { return numbers;}
    }

    public static int getSumBeforeMinAndMax(int[] numbers) {
        if (numbers.length != 0) {
            int sum = 0;
            int min = numbers[0];
            int iMin = 0;
            int max = numbers[0];
            int iMax = 0;
            for (int i = 0; i < numbers.length; i++) {
                if (numbers[i] < min) {
                    min = numbers[i];
                    iMin = i;
                }
                if (numbers[i] > max) {
                    max = numbers[i];
                    iMax = i;
                }
            }
            for (int i = Math.min(iMax,iMin) + 1; i < Math.max(iMax,iMin); i++) {
                sum = sum + numbers[i];
            }
            return sum;
        } else { return 0;}
    }
    // END
}
