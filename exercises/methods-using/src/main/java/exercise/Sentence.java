package exercise;

import java.util.Locale;

class Sentence {
    public static void printSentence(String sentence) {
        // BEGIN
        System.out.println(sentence.charAt(sentence.length() - 1) == '!' ? sentence.toUpperCase() : sentence.toLowerCase());
        // END
    }
}
