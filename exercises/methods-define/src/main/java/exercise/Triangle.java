package exercise;

class Triangle {
    // BEGIN
    public static double getSquare (int a,int b,int ang) {
        return (a * b * 0.5 * Math.sin( Math.PI * ang / 180));
    }

    public static void main (String[] args) {
        System.out.println(getSquare(4,5,45));
    }
    // END
}
