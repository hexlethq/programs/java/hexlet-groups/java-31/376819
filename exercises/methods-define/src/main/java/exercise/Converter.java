package exercise;

class Converter {
    // BEGIN
    public static int convert (int weigth,String format) {
        switch (format) {
            case ("b"):
                return  (weigth * 1024);
            case ("Kb"):
                return (weigth / 1024);
            default:
                return 0;
        }
    }

    public static void main (String[] args) {
        System.out.println("10 Kb = " + convert(10,"b") + " b");
    }
    // END
}
