package exercise;

import java.security.PrivateKey;
import java.util.Arrays;


// BEGIN
public class Kennel {

    private static String[][] puppeys = new String[0][2];
    private static int puppeyNumber = 0;


    public static void addPuppy(String[] puppey) {
        puppeys = Arrays.copyOf(puppeys,puppeyNumber+1);
        puppeys[puppeyNumber] = puppey;
        puppeyNumber++;
    }

    public static void addSomePuppies(String[][] puppies) {
        for (String[] puppey : puppies) {
            addPuppy(puppey);
        }
    }

    public static int getPuppyCount() {
        return puppeyNumber;
    }

    public static boolean isContainPuppy(String puppeyName) {
        for (String[] puppey : puppeys) {
            if (puppey[0].equals(puppeyName)) {
                return true;
            }
        }
        return false;
    }

    public static String[][] getAllPuppies() {
        return puppeys;
    }

    public static String[] getNamesByBreed(String breed) {
        String[] names = new String[puppeyNumber];
        int i = 0;
        for (String[] puppey: puppeys) {
            if (puppey[0] == null) {return names;}
            if (puppey[1].equals(breed)) {
                names[i] = puppey[0];
                i++;
            }
        }
        return Arrays.copyOf(names,i);
    }

    public static void resetKennel() {
        puppeys = new String[0][2];
        puppeyNumber = 0;
    }


    //а что если несколько пупеев с одинаковым пупейнемом
    public static boolean removePuppy(String puppeyName) {
        String[][] newKennel = new String[puppeys.length][2];
        boolean remove = false;
        int i = 0;
        for (String[] puppey : puppeys) {
            //if (puppey[0] == null) {break;}
            if (puppey[0].equals(puppeyName) && !(remove)) { //забирает только 1го щенка
                remove = true;
                continue;
            }
            newKennel[i++] = puppey;
        }
        puppeyNumber = i;
        puppeys = Arrays.copyOf(newKennel,i);
        return remove;
    }


// END

    public static void main(String[] args) {

        String[] puppy1 = {"Rex", "boxer"};
        Kennel.addPuppy(puppy1);
        String[][] puppies2 = {
                {"Buddy", "chihuahua"},
                {"Toby", "chihuahua"},
        };
        Kennel.addSomePuppies(puppies2);

        System.out.println(Kennel.getPuppyCount());

        System.out.println(Kennel.isContainPuppy("Buddy"));

        String[][] puppies = Kennel.getAllPuppies();
        System.out.println(Arrays.deepToString(puppies));

        String[] names = Kennel.getNamesByBreed("chihuahua");
        System.out.println(Arrays.toString(names)); // => [Buddy, Toby]


        System.out.println(Kennel.removePuppy("Toby"));
        System.out.println(Kennel.getPuppyCount());

        System.out.println(Kennel.removePuppy("Toby"));
        System.out.println(Kennel.getPuppyCount());

        Kennel.resetKennel();
        System.out.println(Kennel.getPuppyCount());

    }

}