package exercise;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;

// BEGIN
public class PairedTag extends Tag {
    String body;
    List<Tag> children = new ArrayList<>();;

    PairedTag(String tagName, Map<String, String> attributes, String body, List<Tag> children) {
        super(tagName, attributes);
        this.body = body;
        this.children.addAll(children);
    }

    private String closePairedTag() {
        return "</" + tagName + ">";
    }

    @Override
    public String toString() {
        StringBuilder returnStr = new StringBuilder(super.toString());
        returnStr.append(body);
        for (Tag sTag: children) {
            returnStr.append(sTag.toString());
        }
        returnStr.append(closePairedTag());
        return returnStr.toString();
    }
}
// END
