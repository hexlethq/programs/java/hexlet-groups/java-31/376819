package exercise;

import java.util.LinkedHashMap;
import java.util.stream.Collectors;
import java.util.Map;

// BEGIN
public abstract class Tag {
    String tagName;
    Map<String, String> attributes = new LinkedHashMap<>();

    protected Tag(String tagName, Map<String, String> attributes) {
        this.tagName = tagName;
        this.attributes.putAll(attributes);
    }

    @Override
    public String toString() {
        StringBuilder returnStr = new StringBuilder("<" + tagName);
        for (String key: attributes.keySet()) {
            returnStr.append(" ")
                    .append(key)
                    .append("=\"")
                    .append(attributes.get(key))
                    .append("\"");
        }
        returnStr.append(">");
        return returnStr.toString();
    }
}
// END
