package exercise;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
//import java.util.Locale;

// BEGIN
public class App {

    public static boolean scrabble(String symbols, String word) {
        List<String> symbolsList = new ArrayList<>();
        for (char symbol: symbols.toCharArray()){
            symbolsList.add(String.valueOf(symbol));
        }
        for (char wordSymbol: word.toLowerCase().toCharArray()){
            if (symbolsList.contains(String.valueOf(wordSymbol))) {
                symbolsList.remove(String.valueOf(wordSymbol));
            } else { return false; }
            }
        return true;
    }

}
//END
