package exercise;

import java.util.Arrays;

class Point {
    // BEGIN

    public static int[] makePoint (int x,int y) {
       return new int[] {x,y};
    }

    public static int getX (int[] point) {
        return point[0];
    }

    public static int getY (int[] point) {
        return point[1];
    }

    public static String pointToString (int[] point) {
        return  "(" + getX(point) + ", " + getY(point) + ")";
    }

    public static int getQuadrant (int[] point) {
        if (getX(point) > 0 && getY(point) > 0) {
            return 1;
        } else if (getX(point) < 0 && getY(point) > 0) {
            return 2;
        } else if (getX(point) < 0 && getY(point) < 0) {
            return 3;
        } else if (getX(point) > 0 && getY(point) < 0) {
            return 4;
        } else {return 0;}
    }

    public static int[] getSymmetricalPointByX(int[] point) {
        return new int[] {-getX(point),getY(point)};
    }

    public static double calculateDistance(int[] point1, int[] point2) {
        double distSqX = Math.pow(getX(point1) - getX(point2), 2);
        double distSqY = Math.pow(getY(point1) - getY(point2), 2);
        return Math.pow(distSqX + distSqY, 0.5);
    }
    // END

    //public static void main (String[] args) {
    //    int[] point = makePoint(0,1);
    //    System.out.println(Arrays.toString(point));
    //    System.out.println(getX(point));
    //    System.out.println(getY(point));
    //    System.out.println(pointToString(point));
    //    System.out.println(getQuadrant(point));
    //    System.out.println(Arrays.toString(getSymmetricalPointByX(point)));
    //    int[] point1 = makePoint(3,1);
    //    int[] point2 = makePoint(7,-2);
    //    System.out.println(calculateDistance(point1,point2));
    //}
}
