package exercise;

import org.junit.jupiter.api.Test;
import java.awt.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;

import static exercise.Point.makePoint;
import static exercise.Point.getX;
import static exercise.Point.getY;
import static exercise.Point.getQuadrant;
import static exercise.Point.pointToString;
import static exercise.Point.getSymmetricalPointByX;
import static exercise.Point.calculateDistance;

class PointTest {
    @Test
    void testGetters() {
        int x = 4;
        int y = 5;
        var point = makePoint(x, y);
        assertThat(getX(point)).isEqualTo(x);
        assertThat(getY(point)).isEqualTo(y);
    }

    @Test
    void testToString() {
        var point = makePoint(4, -5);
        String expected = "(4, -5)";
        assertThat(pointToString(point)).isEqualTo(expected);
    }

    @Test
    void testGetQuadrant() {
        int actual1 = getQuadrant(makePoint(2, 3));
        assertThat(actual1).isEqualTo(1);

        int actual2 = getQuadrant(makePoint(-2, 3));
        assertThat(actual2).isEqualTo(2);

        int actual3 = getQuadrant(makePoint(-2, -3));
        assertThat(actual3).isEqualTo(3);

        int actual4 = getQuadrant(makePoint(2, -3));
        assertThat(actual4).isEqualTo(4);

        int actual5 = getQuadrant(makePoint(0, 3));
        assertThat(actual5).isEqualTo(0);

        int actual6 = getQuadrant(makePoint(-2, 0));
        assertThat(actual6).isEqualTo(0);
    }

    // BEGIN
    @Test
    void testGetSymmetricalPointByX() {
        var point1 = makePoint(1,1);
        String expected1 = "(-1, 1)";
        assertThat(pointToString(getSymmetricalPointByX(point1))).isEqualTo(expected1);

        var point2 = makePoint(0,10);
        String expected2 = "(0, 10)";
        assertThat(pointToString(getSymmetricalPointByX(point2))).isEqualTo(expected2);

        var point3 = makePoint(-8,2);
        String expected3 = "(8, 2)";
        assertThat(pointToString(getSymmetricalPointByX(point3))).isEqualTo(expected3);
    }

    @Test
    void testCalculateDistance() {
        var pointA1 = makePoint (0,0);
        var pointB1 = makePoint (0,0);
        double result1 = calculateDistance(pointA1,pointB1);
        assertThat(result1).isCloseTo(0.0, within(0.05));

        var pointA2 = makePoint (-1,0);
        var pointB2 = makePoint (2,4);
        double result2 = calculateDistance(pointA2,pointB2);
        assertThat(result2).isCloseTo(5.0, within(0.05));

        var pointA3 = makePoint (0,18);
        var pointB3 = makePoint (0,14);
        double result3 = calculateDistance(pointA3,pointB3);
        assertThat(result3).isCloseTo(4.0, within(0.05));

        var pointA4 = makePoint (3,3);
        var pointB4 = makePoint (-9,8);
        double result4 = calculateDistance(pointA4,pointB4);
        assertThat(result4).isCloseTo(13.0, within(0.05));
    }
    // END
}
