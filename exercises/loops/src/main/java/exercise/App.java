package exercise;

class App {
    // BEGIN
    public static String getAbbreviation (String phrase) {
        String new_phrase = "";
        phrase = " " + phrase;
        for (int i = 0; i < phrase.length(); i++) {
            if ((phrase.charAt(i) != ' ') && (phrase.charAt(i-1) == ' ')) {
                new_phrase = new_phrase + phrase.charAt(i);
            }
        }
        return new_phrase.toUpperCase();
    }
    // END
}
