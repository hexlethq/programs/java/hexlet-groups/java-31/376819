package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.Map;

import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.Files;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.lang3.ArrayUtils;

public class UsersServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        String pathInfo = request.getPathInfo();

        if (pathInfo == null) {
            showUsers(request, response);
            return;
        }

        String[] pathParts = pathInfo.split("/");
        String id = ArrayUtils.get(pathParts, 1, "");

        showUser(request, response, id);
    }

    private List getUsers() throws JsonProcessingException, IOException {
        // BEGIN
        String filepath = "src/main/resources/users.json";
        String content =Files.readString(Path.of(filepath));
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(content, List.class);
        // END
    }

    private void showUsers(HttpServletRequest request,
                          HttpServletResponse response)
                throws IOException {

        // BEGIN
        StringBuilder html = new StringBuilder();
        html.append("" +
                "<!DOCTYPE html>\n" +
                "<html lang=\"ru\">\n" +
                "    <head>\n" +
                "        <meta charset=\"UTF-8\">\n" +
                "        <title>Example application</title>\n" +
                "        <link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css\"\n" +
                "              rel=\"stylesheet\"\n" +
                "              integrity=\"sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We\"\n" +
                "              crossorigin=\"anonymous\">\n" +
                "    </head>\n");
        html.append("    <body>\n");
        html.append("        <table border=\"1\">\n" +
                "            <tr>\n" +
                "                <th>id</th>\n" +
                "                <th>fullName</th>\n" +
                "            </tr>");
        for (Map map: (List<Map>) getUsers()) {
            html.append("            <tr><td>" + map.get("id")
                    + "</td><td>"
                    + "<a href=\"/users/" + map.get("id") + "\">"
                    + map.get("firstName") + " " + map.get("lastName") + "</a>"
                    + "</td></tr>\n");
        }
        html.append("        </table>\n" +
                "    </body>\n" +
                "</html>");

        PrintWriter pw = response.getWriter();
        pw.write(html.toString());
        // END
    }

    private void showUser(HttpServletRequest request,
                         HttpServletResponse response,
                         String id)
                 throws IOException {

        // BEGIN
        boolean t = false;
        StringBuilder html = new StringBuilder();
        html.append("" +
                "<!DOCTYPE html>\n" +
                "<html lang=\"ru\">\n" +
                "    <head>\n" +
                "        <meta charset=\"UTF-8\">\n" +
                "        <title>Example application</title>\n" +
                "        <link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css\"\n" +
                "              rel=\"stylesheet\"\n" +
                "              integrity=\"sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We\"\n" +
                "              crossorigin=\"anonymous\">\n" +
                "    </head>\n");
        html.append("    <body>\n");
        for (Map map: (List<Map>) getUsers()) {
            if (map.get("id").equals(id)) {
                t = true;
                html.append("        <table border=\"1\">\n" +
                        "            <tr>\n" +
                        "                <th>firstName</th>\n" +
                        "                <th>lastName</th>\n" +
                        "                <th>id</th>\n" +
                        "                <th>email</th>\n" +
                        "            </tr>");
                html.append("            <tr><td>" + map.get("firstName")
                        + "</td><td>" + map.get("lastName")
                        + "</td><td>" + map.get("id")
                        + "</td><td>" + map.get("email") + "</td></tr>");
                html.append("        </table>\n");
            }
        }
        html.append("    </body>\n" +
                "</html>");
        if (t) {
            PrintWriter pw = response.getWriter();
            pw.write(html.toString());
        } else {
            response.sendError(404, "Not found");
        }
            // END
    }
}
