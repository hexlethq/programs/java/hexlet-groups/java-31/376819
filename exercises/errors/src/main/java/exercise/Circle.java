package exercise;

import java.lang.Math;

// BEGIN
public class Circle {
    Point point;
    int radius;
    public static final NegativeRadiusException error = new NegativeRadiusException("Не удалось посчитать площадь");

    Circle(Point point, int radius) {
        this.point = point;
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    public double getSquare() throws NegativeRadiusException {
        if (radius < 0) {
            throw error;
        } else {
            return Math.PI * Math.pow(radius, 2);
        }
    }
}
// END
