package exercise;

import java.util.stream.Collectors;
import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
public class App {
    public static String getForwardedVariables(String content) {

        return Arrays.stream(content.split("\n"))
                .filter(line -> line.startsWith("environment="))
                .map(line -> line.replaceAll("[\"]","").trim()
                        .replaceAll("environment=",""))
                .flatMap(line -> Stream.of(line.split(","))
                        .filter(var -> var.startsWith("X_FORWARDED_"))
                        .map(var -> var.replaceAll("X_FORWARDED_","")))
                .collect(Collectors.joining(","));
    }
}
//END
