// BEGIN
package exercise; //main.java.exercise;

import exercise.geometry.Point;
import exercise.geometry.Segment;
import java.util.Arrays;


class App {
    public static double[] getMidpointOfSegment (double[][] segment) {
        double midX = (Point.getX(Segment.getBeginPoint(segment)) + Point.getX(Segment.getEndPoint(segment))) * 0.5;
        double midY = (Point.getY(Segment.getBeginPoint(segment)) + Point.getY(Segment.getEndPoint(segment))) * 0.5;
        return Point.makePoint(midX,midY);
    }

    public static double[][] reverse (double[][] segment) {
        double[] newBeginPoint = Segment.getEndPoint(segment).clone();
        double[] newEndPoint = Segment.getBeginPoint(segment).clone();
        return Segment.makeSegment(newBeginPoint,newEndPoint);
    }


    public static boolean isBelongToOneQuadrant(double[][] segment) {
        //xy 1oi tochki
        double[] pointA = Segment.getBeginPoint(segment);
        double xA = Point.getX(pointA);
        double yA = Point.getY(pointA);
        //xy 2oi tochki
        double[] pointB = Segment.getEndPoint(segment);
        double xB = Point.getX(pointB);
        double yB = Point.getY(pointB);

        if ((xA > 0 && xB > 0) || (xA < 0 && xB < 0)){
            if ((yA > 0 && yB > 0) || (yA < 0 && yB < 0)){ return true;}
        }
        return false;
    }

    //public static void main(String[] args) {
    //    double[] point1 = Point.makePoint(3, 4);
    //    double[] point2 = Point.makePoint(6, 7);
    //    double[][] segment = Segment.makeSegment(point1, point2);
//
    //    double[] midPoint = App.getMidpointOfSegment(segment);
    //    System.out.println(Arrays.toString(midPoint)); // => [4.5, 5.5]
//
    //    double[][] reversedSegment = App.reverse(segment);
    //    double[] beginPoint = Segment.getBeginPoint(reversedSegment);
    //    double[] endPoint = Segment.getEndPoint(reversedSegment);
    //    System.out.println(Arrays.toString(beginPoint)); // => [6, 7]
    //    System.out.println(Arrays.toString(endPoint)); // => [3, 4]
//
    //}
}
// END
