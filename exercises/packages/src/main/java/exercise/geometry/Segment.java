// BEGIN
package exercise.geometry;

public class Segment {
    public static double[][] makeSegment (double[] pointA, double[] pointB) {
        double[][] segment = {pointA,pointB};
        return segment;
    }

    public static double[] getBeginPoint (double[][] segment) {return segment[0];
    }

    public static double[] getEndPoint (double[][] segment) {return segment[1];
    }
}
// END
