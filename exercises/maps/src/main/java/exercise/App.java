package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
public class App {
    public static Map<String, Integer> getWordCount(String line) {
        Map<String, Integer> wordCount = new HashMap<>();
        if (line.equals("")) { return wordCount; }
        for (String word: line.split(" ")) {
            wordCount.put(word,wordCount.getOrDefault(word,0) + 1);
        }
        return wordCount;
    }


    public static String toString(Map<String, Integer> initMap) {
        String output = "{";
        if (initMap.isEmpty()){
            return "{}";
        }
        for (String key: initMap.keySet()){
                output += "\n  " + key + ": " + initMap.get(key);
            }
            output+= "\n}";
        return output;
    }


}
//END
