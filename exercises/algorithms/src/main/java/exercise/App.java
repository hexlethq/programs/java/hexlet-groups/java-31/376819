package exercise;

import javax.net.ssl.SSLContext;
import java.util.Arrays;

class App {
    // BEGIN
    public static int[] sort (int[] startArr) {
        if (startArr.length != 0) {
            boolean test = true;
            int i = 1;
            while (test){
                test = false;
                for (int j = 0; j < (startArr.length - i); j++) {
                    if (startArr[j] > startArr[j + 1]) {
                        startArr[j] += startArr[j+1];
                        startArr[j + 1] = startArr[j] - startArr[j + 1];
                        startArr[j] = startArr[j] - startArr[j + 1];
                        test = true;
                    }
                }
                i++;
            }
            return startArr;
        }
        return new int[0];
    }

    public static int[] selectSort (int[] startArr) {
        if (startArr.length != 0){
            for (int i = 0; i < (startArr.length - 1); i++) {
                int min = i;
                for (int j = i + 1; j < startArr.length; j++) {
                    if (startArr[j] < startArr[min]) {
                        min = j;
                    }
                }
                if (min != i) {
                    startArr[i] += startArr[min];
                    startArr[min] = startArr[i] - startArr[min];
                    startArr[i] = startArr[i] - startArr[min];
                }
            }
            return startArr;
        }
        return new int[0];
    }
    // END
    //public static void main (String[] args) {
    //    int[] numbers = {};
    //    //System.out.println(Arrays.toString(sort(numbers)));
    //    System.out.println(Arrays.toString(selectSort(numbers)));
    //}
}
