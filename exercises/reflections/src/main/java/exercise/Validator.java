package exercise;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// BEGIN
public class Validator {
    public static List<String> validate(Address address) {
        List<String> fieldList = new ArrayList<>();
        for (Field field: address.getClass().getDeclaredFields()) {
            NotNull nnField = field.getAnnotation(NotNull.class);
            try {
                field.setAccessible(true);
                if (nnField != null && field.get(address) == null) {
                    fieldList.add(field.getName());
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return fieldList;
    }

    public static Map<String, List<String>> advancedValidate(Address address) {
        Map<String, List<String>> fieldMap = new HashMap<>();
        for (Field field: address.getClass().getDeclaredFields()) {
            NotNull nnField = field.getAnnotation(NotNull.class);
            MinLength mlField = field.getAnnotation(MinLength.class);
            List<String> errMsgs = new ArrayList<>();

            try {
                field.setAccessible(true);
                Object fieldValue = field.get(address);
                if (nnField != null &&
                        fieldValue == null) {
                    errMsgs.add("can not be null");
                } else if (mlField != null &&
                        fieldValue.toString().length() < mlField.minLength()) {
                    errMsgs.add("length less than " + mlField.minLength());
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            if (!errMsgs.isEmpty()) {
                fieldMap.put(field.getName(), errMsgs);
            }

        }
        return fieldMap;
    }
}
// END
