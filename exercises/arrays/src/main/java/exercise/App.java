package exercise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class App {
    // BEGIN
    public static int[] reverse (int[] startArray) {
        int[] reArray = new int[startArray.length];
        for (int index=0; index <= startArray.length - 1; index++) {
            reArray[index]=startArray[startArray.length - 1 - index];
        }
        return reArray;
    }

    public static int mult (int[] theArray) {
        int resut = 1;
        for (int value: theArray) {
            resut = resut * value;
        }
        return resut;
    }

    public static int[] flattenMatrix (int[][] table) {
        if (table.length !=0) {
            int[] array = new int[table.length * table[table.length - 1].length];
            int i = 0;
            for (int[] arr: table){
                for (int a: arr) {
                    array[i++] = a;
                    //i++;
                }
            }
            return array;
        } else {
            return new int[0];
        }
    }
    // END
}
