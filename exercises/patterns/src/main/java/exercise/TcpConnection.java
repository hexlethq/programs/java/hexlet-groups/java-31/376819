package exercise;
import exercise.connections.Connection;
import exercise.connections.Disconnected;

import javax.swing.plaf.nimbus.State;
import java.util.List;
import java.util.ArrayList;

// BEGIN
public class TcpConnection {
    private Connection state;
    String address;
    int port;

    public TcpConnection(String address, int port) {
        this.address = address;
        this.port = port;
        this.state = new Disconnected(this);
    }

    public String getCurrentState() {
        return getState().getCurrentState();
    }

    public void connect() {
        getState().connect();
    }

    public void disconnect() {
        getState().disconnect();
    }

    public void write(String data) {
        getState().write(data);
    }

    public Connection getState() {
        return state;
    }

     public void setState(Connection state) {
        this.state = state;
    }
}
// END
