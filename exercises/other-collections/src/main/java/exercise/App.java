package exercise;

import java.util.LinkedHashMap;
import java.util.Map;

// BEGIN
public class App {
    public static Map<String,String> genDiff(Map<String,Object> map1, Map<String,Object> map2) {
        Map<String, String> returnMap = new LinkedHashMap<>();
        for (String key1 : map1.keySet()) {
            for (String key2 : map2.keySet()) {
                if (map1.get(key1).equals(map2.get(key2))) {
                    returnMap.put(key1, "unchanged");
                } else if (key1.equals(key2)) {
                    returnMap.put(key1, "changed");
                }
            }
        }

        for (String key1 : map1.keySet()) {
            returnMap.put(key1, returnMap.getOrDefault(key1, "deleted"));
        }
        for (String key2 : map2.keySet()) {
            returnMap.put(key2, returnMap.getOrDefault(key2, "added"));
        }

        return returnMap;
    }
}
//END
