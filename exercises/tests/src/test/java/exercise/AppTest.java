package exercise;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;

class AppTest {

    @Test
    void testTake() {
        // BEGIN
        List<Integer> list1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6));
        int count1 = 3;
        List<Integer> result1 = new ArrayList<>(App.take(list1, count1));
        List<Integer> expected1 = new ArrayList<>(Arrays.asList(1, 2, 3));
        assertThat(result1).isEqualTo(expected1);
        // END
    }
}
