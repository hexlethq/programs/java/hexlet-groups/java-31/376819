package exercise;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Files;
import java.util.List;

// BEGIN
public class App {
    public static void save(Path path, Car car) throws Exception {
        List<String> listCar = List.of(car.serialize());
        try {
            Files.write(path, listCar);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Car extract(Path path) throws Exception {
        String carString = "";
        try {
            carString = Files.readString(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Car.unserialize(carString);
    }

}
// END
