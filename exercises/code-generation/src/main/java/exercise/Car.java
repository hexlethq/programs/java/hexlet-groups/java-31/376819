package exercise;

import lombok.Value;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import java.util.Map;

// BEGIN
@Value
// END
class Car {
    int id;
    String brand;
    String model;
    String color;
    User owner;

    // BEGIN
    public String serialize() throws Exception {
        Map mapCar = new LinkedHashMap();
        for (Field field: this.getClass().getDeclaredFields()) {
            try {
                field.setAccessible(true);
                mapCar.put(field.getName(), field.get(this));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(mapCar);
    }

    public static Car unserialize(String jsonCar) throws Exception {
        ObjectMapper mapper = new ObjectMapper(new JsonFactory());
        Map mapCar = mapper.readValue(jsonCar, Map.class);

        Map userMap = (Map) mapCar.get("owner");
        User user = new User((int) userMap.get("id"),
                (String) userMap.get("firstName"),
                (String) userMap.get("lastName"),
                (int) userMap.get("age"));

        return new Car((int) mapCar.get("id"),
                (String) mapCar.get("brand"),
                (String) mapCar.get("model"),
                (String) mapCar.get("color"),
                user);
    }
    // END
}
