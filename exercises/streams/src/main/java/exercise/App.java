package exercise;

import java.util.List;
import java.util.Arrays;

// BEGIN
public class App {
    public static long getCountOfFreeEmails(List<String> emails) {
        return emails.stream()
                .map(email -> (email.split("@"))[1])
                .filter(domain -> domain.matches("gmail.com|yandex.ru|hotmail.com"))
                .count();
    }
}
// END
