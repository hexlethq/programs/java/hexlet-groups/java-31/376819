package exercise.servlet;

import exercise.Data;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.stream.Collectors;
import static exercise.Data.getCompanies;

public class CompaniesServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        // BEGIN
        List<String> companies = getCompanies();
        PrintWriter pw = response.getWriter();
        String p = request.getParameter("search");
        if (p != null && !p.isEmpty()) {
            companies = companies.stream()
                    .filter(c -> c.contains(p))
                    .collect(Collectors.toList());
        }
        if (companies.size() != 0) {
            companies.forEach(c -> pw.write(c + "\n"));
        } else {
            pw.write("Companies not found");
        }
        // END
    }
}
