package exercise;

import java.util.List;
import java.util.stream.Collectors;
import static java.lang.Math.min;

// BEGIN
public class App {
    public static List<String> buildAppartmentsList(List<Home> appartments, int n) {
        return appartments.stream()
                .sorted(Home::compareTo)
                .map(Object::toString)
                .collect(Collectors.toList())
                .subList(0, min(appartments.size(), n));
    }

}
// END
