package exercise;

// BEGIN
public class Cottage implements Home {
    double area;
    int floorCount;

    public Cottage() {}

    public Cottage(double area, int floorCount) {
        this.area = area;
        this.floorCount = floorCount;
    }

    @Override
    public String toString() {
        return String.format("%s этажный коттедж площадью %s метров", this.floorCount, this.getArea());
    }

    @Override
    public double getArea() {
        return area;
    }

    @Override
    public int compareTo(Home anotherHome) {
        return Double.compare(this.getArea(), anotherHome.getArea());
    }
}
// END
