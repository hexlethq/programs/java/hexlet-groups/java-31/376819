package exercise;

// BEGIN
public class ReversedSequence implements CharSequence {
    String string;

    public ReversedSequence(String string) {
        this.string = string;
    }

    @Override
    public String toString() {
        StringBuilder reversedString = new StringBuilder();
        for (int i = 0; i < this.length(); i++) {
            reversedString.append(this.charAt(i + 1));
        }
        return reversedString.toString();
    }

    @Override
    public int length() {
        return string.length();
    }

    @Override
    public char charAt(int index) {
        return string.charAt(string.length() - index);
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        StringBuilder subString = new StringBuilder();
        for (int i=start; i < end; i++) {
            subString.append(this.charAt(i + 1));
        }
        CharSequence cs = subString.toString();
        return cs;
    }
}
// END
