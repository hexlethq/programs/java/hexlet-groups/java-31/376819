<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- BEGIN -->
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>Example application</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css"
              rel="stylesheet"
              integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We"
              crossorigin="anonymous">
    </head>
    <body>
        <table border="1">
            <tr>
                <th>firstName</th>
                <th>lastName</th>
                <th>id</th>
                <th>email</th>
            </tr>
            <tr>
                <td>${user.get("firstName")}</td>
                <td>${user.get("lastName")}</td>
                <td>${user.get("id")}</td>
                <td>${user.get("email")}</td>
            </tr>
        </table>
        <form action="/users/delete" method="get">
            <input type="hidden" name="id" value="${user.get("id")}">
            <input type="submit" value="delete">
        </form>
    </body>
</html>
<!-- END -->
