<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- BEGIN -->
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>Example application</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css"
              rel="stylesheet"
              integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We"
              crossorigin="anonymous">
    </head>
    <body>
        <h2>
            Are you shure, that you want delete
            <a href="/users/show?id=${user.get("id")}">${user.get("firstName")} ${user.get("lastName")}</a>
            ?
        </h2>
        <form action="" method="post">
            <input type="submit" value="Delete">
        </form>
    </body>
</html>
<!-- END -->
