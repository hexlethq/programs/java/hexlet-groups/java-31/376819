package exercise;

import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.Map;
import java.util.List;
import java.time.LocalDate;
import java.util.stream.Collectors;

// BEGIN
public class Sorter {
    public static List<String> takeOldestMans(List<Map<String,String>> people) {
        return people.stream()
                .filter(person -> person.get("gender").equals("male"))
                .sorted(Comparator.comparing(person ->  -ageInDays(person.get("birthday"))))
                .map(person -> person.get("name"))
                .collect(Collectors.toList());
    }

    public static int ageInDays (String birthday) {
        return (int) ChronoUnit.DAYS.between(LocalDate.parse(birthday), LocalDate.now());
    }
}
// END
