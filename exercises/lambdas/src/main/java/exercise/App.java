package exercise;

import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
public class App {
    public static String[][] enlargeArrayImage(String[][] startArray) {

        return Arrays.stream(startArray)
                .map( line -> Arrays.stream(line).flatMap(val -> Stream.of( val, val)).toArray(String[]::new))
                .flatMap( line -> Stream.of(line, line))
                .toArray(String[][]::new);
    }
}
// END
