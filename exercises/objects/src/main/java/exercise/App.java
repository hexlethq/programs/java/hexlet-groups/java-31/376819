package exercise;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

class App {
    // BEGIN
    public static String buildList (String[] animals) {
        if (animals.length != 0) {
            StringBuilder full = new StringBuilder();
            full.append("<ul>\n");
            for (String name : animals) {
                full.append("  <li>");
                full.append(name);
                full.append("</li>\n");
            }
            full.append("</ul>");
            return full.toString();
        } else {return "";}
    }

    public static String getUsersByYear (String[][] users, int yy) {
        if (users.length != 0) {
            int i = 0;
            StringBuilder full = new StringBuilder();
            full.append("<ul>\n");
            for (String[] name : users) {
                if (LocalDate.parse(name[1]).getYear() == yy) {
                    full.append("  <li>");
                    full.append(name[0]);
                    full.append("</li>\n");
                    i++;
                }
            }
            full.append("</ul>");
            if (i != 0) {
                return full.toString();
            } else {return "";}
        } else {return "";}
    }
    // END


    //public static void main (String[] args) throws Exception {
    //    String[][] users = {
    //            {"Andrey Petrov", "1990-11-23"},
    //            {"Aleksey Ivanov", "1995-02-15"},
    //            {"Anna Sidorova", "1996-09-09"},
    //            {"John Smith", "1990-03-11"},
    //            {"Vanessa Vulf", "1985-11-16"},
    //            {"Vladimir Nikolaev", "1990-12-27"},
    //            {"Alice Lucas", "1986-01-01"},
    //            {"Elsa Oscar", "2000-03-25"},
    //    };
    //    System.out.println(getYoungestUser(users,"15 Feb 1965"));
    //}

    // Это дополнительная задача, которая выполняется по желанию.
    public static String getYoungestUser(String[][] users, String date) throws Exception {
        // BEGIN
        String name = "";
        LocalDate minDate = LocalDate.MIN;
        DateTimeFormatter pattern = DateTimeFormatter.ofPattern("dd MMM yyyy", Locale.ENGLISH);
        LocalDate dateBefore = LocalDate.parse(date,pattern);
        for (String[] user: users){
            if ((minDate.isBefore(LocalDate.parse(user[1]))) && LocalDate.parse(user[1]).isBefore(dateBefore)) {
                minDate = LocalDate.parse(user[1]);
                name = user[0];
            }
        }
        return name;
        // END
    }
}
